import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
import Card from 'src/components/CardImovel.vue'
import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'CarrosselCards',
  mixins: [Utilidades],
  methods: {
    moverEsquerda () {
      if (this.$refs.c1) {
        this.$refs.c1.next()
      }
    },
    moverDireita () {
      if (this.$refs.c1) {
        this.$refs.c1.prev()
      }
    }
  },
  data () {
    return {
      slide: 1,
      fullScreen: false
    }
  },
  computed: {
    settings () {
      if (this.configs) {
        return this.configs
      } else {
        return {
          dots: false,
          arrows: true,
          infinite: false,
          speed: 500,
          slidesToShow: 3,
          swipe: true,
          centerMode: false,
          slidesToScroll: 3,
          initialSlide: 0,
          responsive: [
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 1100,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 720,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                swipe: true
              }
            },
            {
              breakpoint: 470,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                swipe: true
              }
            }
          ]
        }
      }
    }
  },
  props: {
    cards: {
      type: Array,
      default: () => ['']
    },
    width: {
      type: String,
      default: '276px'
    },
    configs: {
      type: Object
    },
    margemBotoes: {
      type: String,
      default: '10px'
    }
  },
  components: {
    VueSlickCarousel,
    Card
  }
}
