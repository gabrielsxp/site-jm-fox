import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'

export default {
  name: 'CarrosselAnuncio',
  data () {
    return {
      slide: 1,
      fullScreen: false
    }
  },
  props: {
    imgs: {
      type: Array,
      default: () => ['']
    }
  },
  methods: {
    imgcarousel (index) {
      if (this.$refs.c1) {
        this.$refs.c1.goTo(index)
      }
      if (this.$refs.c2) {
        this.$refs.c2.goTo(index)
      }
      if (this.$refs.carrosselPrincipal) {
        this.$refs.carrosselPrincipal.goTo(index)
      }
    },
    selecionarImagem (index) {
      if (window.innerWidth <= 767) {
        this.fullScreen = false
      } else {
        this.fullScreen = true
      }
      this.$nextTick(() => {
        this.$refs.c1.goTo(index)
        this.$refs.c2.goTo(index)
      })
    },
    deslizarSlidePrincipal (newVal, oldVal) {
      console.warn('slider: ', newVal, oldVal)
      if (newVal < oldVal) {
        this.$refs.carrosselPrincipal.goTo(newVal)
        this.slide = newVal
        this.$refs.carrosselPrincipal.next()
      }
      if (newVal > oldVal) {
        this.$refs.carrosselPrincipal.goTo(newVal)
        this.slide = newVal
        this.$refs.carrosselPrincipal.prev()
      }
    },
    deslizar (antes, depois) {
      this.slide = depois
    }
  },
  computed: {
    settings () {
      return {
        dots: false,
        lazyLoad: 'ondemand',
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        swipe: true,
        centerMode: false,
        slidesToScroll: 1,
        initialSlide: 0
      }
    },
    carrossel2 () {
      return {
        slidesToShow: 4,
        responsive: [
          {
            breakpoint: 1000,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 599,
            settings: {
              slidesToShow: 3
            }
          }
        ]
      }
    }
  },
  components: {
    VueSlickCarousel
  }
}
