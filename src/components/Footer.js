import RedesSociais from 'src/components/RedesSociais.vue'
import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'Footer',
  mixins: [Utilidades],
  components: {
    RedesSociais
  },
  methods: {
    openUrl (url, target) {
      if (target) window.open(url, '_blank')
      else window.open(url)
    }
  }
}
