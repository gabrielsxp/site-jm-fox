
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/Index.vue') },
      { path: '/contato', name: 'contato', component: () => import('pages/Contato/Contato.vue') },
      { path: '/quem-somos', name: 'quem_somos', component: () => import('pages/QuemSomos/QuemSomos.vue') },
      { path: '/anuncio/:id', name: 'anuncio', component: () => import('pages/Anuncio/Anuncio.vue') },
      { path: '/busca', name: 'busca', component: () => import('pages/Busca/Busca.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
