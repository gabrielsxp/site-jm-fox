export default {
  methods: {
    irRota (name, params = null) {
      if (params) {
        this.$router.push({ name, params })
      } else {
        this.$router.push({ name })
      }
    }
  }
}
