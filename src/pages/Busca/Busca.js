import Header from 'src/components/Header.vue'
import Filtros from 'src/components/Filtros.vue'
import CardImovel from 'src/components/CardImovel.vue'

export default {
  name: 'Busca',
  data () {
    return {
      filtros: false
    }
  },
  components: {
    Header,
    Filtros,
    CardImovel
  },
  computed: {
    tamanhoJanela () {
      if (process.browser) {
        return window.innerWidth
      } else {
        return 300
      }
    }
  }
}
