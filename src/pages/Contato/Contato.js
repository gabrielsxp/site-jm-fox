import Header from 'src/components/Header.vue'
import CardImovel from 'src/components/CardImovel.vue'
import CarrosselCards from 'src/components/CarrosselCards.vue'

export default {
  name: 'Contato',
  components: {
    Header,
    CardImovel,
    CarrosselCards
  },
  methods: {
    enviar () {
      //
    },
    validarEmail (email) {
      // eslint-disable-next-line no-useless-escape
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return re.test(String(email).toLowerCase())
    }
  }
}
